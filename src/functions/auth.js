const jwt = require('jsonwebtoken')
const { getsecretKey } = require('../services/aws-sevice')

async function validate(event) {
  console.log('Event: ', event)
  const authorizerToken = event.authorizationToken
  if (!authorizerToken) return generatePolicy('undefined', 'Deny', event.methodArn)
  const authorizerArr = authorizerToken.split(' ')
  const token = authorizerArr[1]
  const secretKey = await getsecretKey()

  if (authorizerArr.length !== 2 || authorizerArr[0] !== 'Bearer' || authorizerArr.length == 0) {
    return generatePolicy('undefined', 'Deny', event.methodArn)
  }
  
  let decodedJwt = jwt.verify(token, secretKey)
  
  if (typeof decodedJwt.account !== 'undefined') {
    // FAZER A BUSCA NO DYNAMODB TORO-AUTH
    return generatePolicy(decodedJwt.account, 'Allow', event.methodArn)
  }
  return generatePolicy('undefined', 'Deny', event.methodArn)
}

const generatePolicy = function (principalId, effect, resource) {
  let authResponse = {};

  authResponse.principalId = principalId;
  if (effect && resource) {
    let policyDocument = {};
    policyDocument.Version = '2012-10-17';
    policyDocument.Statement = [];
    let statementOne = {};
    statementOne.Action = 'execute-api:Invoke';
    statementOne.Effect = effect;
    statementOne.Resource = resource;
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }
  return authResponse;
}

module.exports = {
  validate
}
