const AWS = require('aws-sdk');
const region = process.env.AWS_DEPLOY_REGION

const secretMng = new AWS.SecretsManager({ region: region })

if (process.env.IS_LOCAL || process.env.IS_OFFLINE || process.env.SERVERLESS_TEST_ROOT || process.env.NODE_ENV === 'test') {
  const credentials = new AWS.SharedIniFileCredentials({ profile: 'Roberlove' });
  AWS.config.credentials = credentials;
}
function getSecretMng (data) {
  return secretMng.getSecretValue({ SecretId: data }).promise()
}
module.exports = {
  getSecretMng
}
