'use strict';
const awsApi = require('../apis/aws-api');

async function getsecretKey () {
  return awsApi.getSecretMng(process.env.SECRET_NAME)
    .then(res => JSON.parse(res.SecretString)[`secretKey-${process.env.STAGE}`])
}

module.exports = {
  getsecretKey
}
